# License #
 https://opensource.org/licenses/GPL-3.0


# README #

This is a password hashing/validation utility 

### What is this repository for? ###

* Quick summary: this is a password hashing/validation utility 
* Version: 1.0-SNAPSHOT


### Usage ###

ExtropyMessageDigest.getSalt() : Gives you a salt (base64 encoded). you must save it for later use.

ExtropyMessageDigest.getPasswordHash(String salt, int iteration, String passwdTxt) : iteration can be supplied by you. Needed for validation. Base64 hash String for you to save instead of password. 

ExtropyMessageDigest.validatePassword (String txtPassword, String passwdToMatch,
                                                String salt, int iteration)

 

txtPassword - Plain text password
passwdToMatch - base 64 password hash you received when called getPasswordHash
salt - base 64 string you received during getSalt()
iteration - saved iteration during getPasswordHash



### How do I get set up? ###

* Summary of set up
* Configuration: JDK 1.7 or above
* Dependencies: Apache Common Language. Check POM
* Database configuration
* How to run tests: mvn clean install
* Deployment instructions : clone, mvn package


### Who do I talk to? ###

* Repo owner or admin: Pratyushr@gmail.com