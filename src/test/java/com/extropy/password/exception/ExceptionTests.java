package com.extropy.password.exception;

import junit.framework.TestCase;
import org.junit.Test;


/**
 * User: pratyushr
 * Date: 9/4/16
 * Time: 10:51 AM
 */
public class ExceptionTests extends TestCase {

    @Test
    public void testNullPasswordException() {
        Exception e = new NullPasswordException();
        assertEquals(e.getMessage(), NullPasswordException.MESSAGE);
    }
    @Test
    public void testPasswordRuleException() {
       Exception e = new PasswordRuleException();
       assertEquals(e.getMessage(), PasswordRuleException.MESSAGE);
    }
}
