package com.extropy.password.digest;


import com.extropy.password.exception.EncodingException;
import junit.framework.Assert;
import junit.framework.TestCase;
import org.junit.Test;

import java.security.NoSuchAlgorithmException;
import java.util.Base64;

/**
 * User: pratyushr
 * Date: 9/4/16
 * Time: 11:04 AM
 */
public class ExtropyMessageDigestTest extends TestCase {

    private static final String passwordToTest = "Pratyush";
    private static final int iteration  = 5;
    private static final String salt = Base64.getEncoder().encodeToString("MY_SALT".getBytes());




    @Test
    public void testSaltRandom() {
        try {
            Assert.assertFalse(ExtropyMessageDigest.getSalt().equals(ExtropyMessageDigest.getSalt()));
        } catch (NoSuchAlgorithmException e) {
            fail();
        }
    }

    @Test
    public void testPasswordHash() {
        try {
            String pwd1 = ExtropyMessageDigest.getPasswordHash(salt, iteration, passwordToTest);
            String pwd2 = ExtropyMessageDigest.getPasswordHash(salt, iteration, passwordToTest);
             Assert.assertEquals(pwd1,pwd2);
        } catch (Exception e) {
              fail();
        }
    }
    @Test
    public void testPasswordHashNoIteration() {
        try {
           String pwd1 = ExtropyMessageDigest.getPasswordHash(salt, passwordToTest);
           String pwd2 = ExtropyMessageDigest.getPasswordHash(salt, passwordToTest);
            Assert.assertEquals(pwd1,pwd2);
        } catch (Exception e) {
             fail();
        }
    }

    @Test
    public void testPasswordHashDiffIteration() {
        try {
            String pwd1 = ExtropyMessageDigest.getPasswordHash(salt, iteration, passwordToTest);
            String pwd2 = ExtropyMessageDigest.getPasswordHash(salt, 3, passwordToTest);
             Assert.assertFalse(pwd1.equals(pwd2));
        } catch (Exception e) {
             fail();
        }
    }

    @Test
    public void testPasswordHashDiffSalt() {
        try {
            String pwd1 = ExtropyMessageDigest.getPasswordHash(salt, iteration, passwordToTest);
            String newSalt = Base64.getEncoder().encodeToString("NEW_SALT".getBytes());
            String pwd2 = ExtropyMessageDigest.getPasswordHash(newSalt, iteration, passwordToTest);
            Assert.assertFalse(pwd1.equals(pwd2));
        } catch (Exception e) {
             fail();
        }
    }

    @Test
    public void testNonBase64Salt() {
        try {
            String pwd1 = ExtropyMessageDigest.getPasswordHash("NON_BASE64", iteration, passwordToTest);
            fail();
        } catch (Exception e) {
           if (e instanceof EncodingException){
               fail();
           }
        }
    }



}
