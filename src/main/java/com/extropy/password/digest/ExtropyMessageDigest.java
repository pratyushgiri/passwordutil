package com.extropy.password.digest;

import com.extropy.password.exception.EncodingException;
import com.extropy.password.exception.NullPasswordException;
import com.extropy.password.exception.PasswordRuleException;
import com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.util.Base64;
/**
 * User: pratyushr
 * Date: 8/24/16
 * Time: 12:37 AM
 */
public class ExtropyMessageDigest {
    private static final Logger logger = LoggerFactory.getLogger(ExtropyMessageDigest.class);

    private static final int KEY_LENGTH = 8 * 120;//120 bytes
    private static final int ONE = 1;

    /**
     * Gives a base 64 encoded String as salt (it is randomized). Users should save the salt as it is required to validateUserPwd the
     * Password.
     * @return
     * @throws NoSuchAlgorithmException
     */
    public static String getSalt() throws NoSuchAlgorithmException {
        return Base64.getEncoder().encodeToString(getSaltBytes()) ;
    }

    /**
     * returns bytes[]
     * @return
     * @throws NoSuchAlgorithmException
     */
    private  static byte[] getSaltBytes() throws NoSuchAlgorithmException {
            SecureRandom secureRandom = SecureRandom.getInstance("SHA1PRNG");
            byte[] salt = new byte[16];
            secureRandom.nextBytes(salt);
            return salt ;
    }

    /**
     * Hashes a plain text string with salt, and # of iterations
     * @param salt        Salt used for password Hash
     * @param iteration   How many iterations the password needs to be hashed.
     * @param passwdTxt   Plain Text to hash
     * @return            A Base64 string containing the hash password
     * @throws NullPasswordException
     * @throws PasswordRuleException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    public static String getPasswordHash(String salt, int iteration, String passwdTxt)
            throws NullPasswordException, PasswordRuleException, NoSuchAlgorithmException, InvalidKeySpecException, EncodingException {

        //input Validation
        validateUserPwd(passwdTxt);
        validateBase64(salt);

        byte[] passwordBytes =  getPasswordHashBytes(salt, iteration, passwdTxt);
        return  (Base64.getEncoder().encodeToString(passwordBytes));
    }

    /**
     * Returns Bytes. Internal Method
     * @param salt
     * @param iteration
     * @param passwdTxt
     * @return
     * @throws NullPasswordException
     * @throws PasswordRuleException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    private static byte[]  getPasswordHashBytes(String salt, int iteration, String passwdTxt)
            throws NullPasswordException, PasswordRuleException, NoSuchAlgorithmException, InvalidKeySpecException {
                //get the bytes from the salt.
        byte[] saltBytes =   Base64.getDecoder().decode(salt);

        //create a PBKeySpec.
        PBEKeySpec specs = new PBEKeySpec(passwdTxt.toCharArray(), saltBytes, iteration, KEY_LENGTH);

        //Secure it
        SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        byte[] passwordBytes = factory.generateSecret(specs).getEncoded();

        return  (passwordBytes);
    }

    /**
     * Hashes a plain text string with salt and 1 iteration
     * @param salt        Salt used for password Hash
     * @param passwdTxt   Plain Text to hash
     * @return            A Base64 string containing the hash password      * @throws NullPasswordException
     * @throws PasswordRuleException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    public static String getPasswordHash(String salt, String passwdTxt)
            throws NullPasswordException, PasswordRuleException, NoSuchAlgorithmException, InvalidKeySpecException, EncodingException {

        return getPasswordHash(salt, ONE, passwdTxt);
    }

    /**
     * Validates if a password is good with the hashed value, passing iteration and salt.
     * @param txtPassword   Plain-text Password the user entered≥
     * @param passwdToMatch Saved Hashed Password
     * @param salt          Salt used during the original Hash
     * @param iteration     Iteration used during the original Hash
     * @return              whether or not the password validation is successful.
     */
    public static boolean validatePassword (String txtPassword, String passwdToMatch,
                                                String salt, int iteration) throws EncodingException {
        if (txtPassword == null || passwdToMatch == null) {
            return false;
        }
        //convert to Bytes from Base 64.
        validateBase64(passwdToMatch);
        byte[] passwordToMatchBytes = Base64.getDecoder().decode(passwdToMatch);

        boolean isValidated = false;
        try {
             isValidated = validatePassword(txtPassword, passwordToMatchBytes, salt, iteration);
        } catch (Exception e) {
            //Any exception is a failure for validation.
            logger.error("Caught Excption:{} while validing password.", e);
            e.printStackTrace();
        }
        return isValidated;
    }

    /**
     *
     * @param txtPassword
     * @param passwdToMatch
     * @param salt
     * @param iteration
     * @return
     * @throws InvalidKeySpecException
     * @throws NullPasswordException
     * @throws NoSuchAlgorithmException
     * @throws PasswordRuleException
     */
    private static boolean validatePassword (String txtPassword, byte[] passwdToMatch,
                                            String salt, int iteration)
            throws InvalidKeySpecException, NullPasswordException, NoSuchAlgorithmException, PasswordRuleException {

        //get Bytes

        byte[] testHashBytes = getPasswordHashBytes(salt, iteration, txtPassword);

        //check diff with BIT operations
        int diff = passwdToMatch.length ^ testHashBytes.length;

        //finish the loop. In this case we will not be using fail-fast philosophy.
        for (int i = 0; i < passwdToMatch.length && i < testHashBytes.length; i++ ){
            diff |= passwdToMatch[i] ^ testHashBytes[i];
        }
        return diff == 0;
    }

    /**
     * Validate if the input password (user entered) passes the criteria
     * @param passwdTxt
     * @throws NullPasswordException
     * @throws PasswordRuleException
     * @throws EncodingException
     */
    private static void validateUserPwd(String passwdTxt)
            throws NullPasswordException, PasswordRuleException {

        //password null
        if (Strings.isNullOrEmpty(passwdTxt)) {
            throw new NullPasswordException();
        }

        if (passwdTxt.length() <= 4) {
            throw new PasswordRuleException();
        }
   }

    private static void validateBase64(String s) throws  EncodingException {
         //check if the salt is Base 64
         if (s == null || !org.apache.commons.codec.binary.Base64.isBase64(s.getBytes())) {
            throw new EncodingException();
         }
    }

}
