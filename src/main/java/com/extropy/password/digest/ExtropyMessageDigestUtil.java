package com.extropy.password.digest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * User: pratyushr
 * Date: 8/24/16
 * Time: 12:07 AM
 */
public class ExtropyMessageDigestUtil {
    private static final Logger logger = LoggerFactory.getLogger(ExtropyMessageDigestUtil.class);

    private static ThreadLocal<MessageDigest> messageDigest = new ThreadLocal<MessageDigest>() {
        @Override
        protected  MessageDigest initialValue() {
            try {
                return MessageDigest.getInstance("SHA");
            } catch (NoSuchAlgorithmException ex) {
                logger.error("Error while getting an instance of Message Digest");
                throw new RuntimeException(ex);
            }
        }
    };



}
