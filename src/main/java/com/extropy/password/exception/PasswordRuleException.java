package com.extropy.password.exception;

/**
 * This exception is thrown when a password does not meet the password acceptance criteria
 * User: pratyushr
 * Date: 8/24/16
 * Time: 12:29 AM
 */
public class PasswordRuleException extends Exception {
    public static final String MESSAGE = "Password does not match Acceptance criteria";
    public PasswordRuleException(){
           super (MESSAGE);
    }
}
