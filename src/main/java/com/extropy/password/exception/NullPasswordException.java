package com.extropy.password.exception;

/**
 * This exception is thrown when a password is Null or Empty
 * User: pratyushr
 * Date: 8/24/16
 * Time: 12:21 AM
 */
public class NullPasswordException extends Exception {
   public static final String MESSAGE =  "Password can't be null.";
   public NullPasswordException() {
       super(MESSAGE);
   }
}
