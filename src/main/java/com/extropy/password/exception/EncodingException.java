package com.extropy.password.exception;

/**
 * User: pratyushr
 * Date: 9/4/16
 * Time: 11:41 AM
 */
public class EncodingException extends Exception {
    public static final String MESSAGE = "Not a Base 64 encoded String";
        public EncodingException(){
               super (MESSAGE);
        }
}
